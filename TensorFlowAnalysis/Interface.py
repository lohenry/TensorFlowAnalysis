# Copyright 2017 CERN for the benefit of the LHCb collaboration
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

import tensorflow as tf
import numpy as np
import itertools

fptype = tf.float64
ctype = tf.complex128

def SetSinglePrecision() : 
  global fptype, ctype
  fptype = tf.float32
  ctype = tf.complex64

def SetDoublePrecision() : 
  global fptype, ctype
  fptype = tf.float64
  ctype = tf.complex128

def FPType() : 
  global fptype
  return fptype

def CType() : 
  global ctype
  return ctype

# Sum of the list components
def Sum( ampls ) : return tf.add_n( ampls )

# Density for a complex amplitude
def Density(ampl) : return tf.abs(ampl)**2

# Absolute value
def Abs(a) : return tf.abs(a)

def Max(x,y) : return tf.maximum(x,y)

def Min(x,y) : return tf.minimum(x,y)

# Create a complex number from real and imaginary parts
def Complex(re, im) : return tf.complex(re, im)

# Create a complex number from a magnitude and a phase
def Polar(a, ph) : return tf.complex(a*tf.cos(ph), a*tf.sin(ph))

# Cast a real number to complex
def CastComplex(re) : return tf.cast(re, dtype = CType() )

# Cast a number to real
def CastReal(re) : return tf.cast(re, dtype = FPType() )

# Return complex conjugate
def Conjugate(c) : return tf.conj(c)

# Return the real part of a complex number
def Real(c): return tf.real(c)

# Return the imaginary part of a complex number
def Imaginary(c): return tf.imag(c)

# Declare constant
def Const(c) : return tf.constant(c, dtype = FPType() )

# Declare invariant
def Invariant(c) : return tf.constant([c], dtype = FPType() )

# Sqrt
def Sqrt(x) : return tf.sqrt(x)

# Exp
def Exp(x) : return tf.exp(x)

# Log
def Log(x) : return tf.log(x)

# Sin
def Sin(x) : return tf.sin(x)

# Cos
def Cos(x) : return tf.cos(x)

# Asin
def Asin(x) : return tf.asin(x)

# Atan
def Atan(x) : return tf.atan(x)

# Acos
def Acos(x) : return tf.acos(x)

# Tanh
def Tanh(x) : return tf.tanh(x)

# Pow
def Pow(x,p): return tf.pow(x,p)

# Pi
def Pi() : return Const( np.pi )

# Create a tensor with zeros of the same shape as input tensor
def Zeros(x) : return tf.zeros_like(x)

# Create a tensor with ones of the same shape as input tensor
def Ones(x) : return tf.ones_like(x)

# Atan2 
def Atan2(y, x) : return tf.atan2(y, x)

# Return argument of a complex number
def Argument(c) : return Atan2(tf.imag(c), tf.real(c))

def Clebsch(j1, m1, j2, m2, J, M) : 
  """
    Return Clebsch-Gordan coefficient. Note that all arguments should be multiplied by 2
    (e.g. 1 for spin 1/2, 2 for spin 1 etc.). Needs sympy.
  """
  from sympy.physics.quantum.cg import CG
  from sympy import Rational
  return CG(Rational(j1,2), Rational(m1,2), Rational(j2,2), Rational(m2,2), Rational(J,2), Rational(M,2) ).doit().evalf()

def Interpolate(t, c) :
  """
    Multilinear interpolation on a rectangular grid of arbitrary number of dimensions
      t : TF tensor representing the grid (of rank N)
      c : Tensor of coordinates for which the interpolation is performed
      return: 1D tensor of interpolated values
  """
  rank = len(t.get_shape())
  ind = tf.cast(tf.floor(c), tf.int32)
  t2 = tf.pad(t, rank*[[1,1]], 'SYMMETRIC')
  wts = []
  for vertex in itertools.product([0, 1], repeat = rank) :
    ind2 = ind + tf.constant(vertex, dtype = tf.int32)
    weight = tf.reduce_prod(1. - tf.abs(c - tf.cast(ind2, dtype = FPType() )), 1)
    wt = tf.gather_nd(t2, ind2+1)
    wts += [ weight*wt ]
  interp = tf.reduce_sum(tf.stack(wts), 0)
  return interp

def SetSeed(seed) :
  """
    Set random seed for numpy
  """
  np.random.seed(seed)
  tf.set_random_seed(seed)

def Random(size) : 
  """
    Generate array of random numbers, uniform from 0 to 1
  """
  return np.random.uniform(0., 1., size).astype('d')
